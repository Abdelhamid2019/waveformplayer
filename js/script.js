// the files we want to play. I choose in this case 3 of my favorite songs

var audios = [
  "media/halo.mp3",
  "media/shawn-mendes-treat-you-better.mp3",
  "media/Kygo ft. Parson James - Stole the show.m4a"
];

// the images of the background associated with each audio,
var cards = [
  {
    background: "media/halo.jpg",
    artist: "Beyoncé",
    thumbnail: "media/beyonce.jpg",
    music: "Halo"
  },
  {
    background: "media/shawn-mendes-treat-you-better.jpg",
    artist: "shawn mendes",
    thumbnail: "media/shawnMendes.jpg",
    music: "Treat you better"
  },
  {
    background: "media/Kygo ft. Parson James - Stole the show.jpg",
    artist: "Kygo",
    thumbnail: "media/kygo.jpg",
    music: "Stole the show"
  }
];

// the iterator over the list.
var audioIndex = 0;

// here we create an audio object
var audio = new Audio();
audio.style.width = "100%";

// we use this function to load our audio file when the window is loaded
function loadAudio() {
  audio.src = audios[audioIndex];
  // we change the music background the title and the artist name
  get("background").src = cards[audioIndex]["background"];
  document.querySelector("#avatar img").src = cards[audioIndex]["thumbnail"];
  get("artistName").innerHTML = cards[audioIndex]["artist"];
  var mn = get("musicName");
  var mmn = get("MobileMusicName");
  if (mn != null) mn.innerHTML = cards[audioIndex]["music"];
  if (mmn != null) mmn.innerHTML = cards[audioIndex]["music"];
  audio.play();
}

// this function is used to switch the button from play to pause & inversely from pause to play
function playOrPause() {
  var animationValidation = pause => {
    if (pause) {
      for (i = 0; i < audio["duration"]; i++) {
        if (get(i)) get(i).style.animation = null;
      }
    } else {
      for (i = 0; i <= audio.currentTime; i++) {
        get(i).style.animation =
          "kf" +
          i +
          " " +
          (Math.random(0, 1) * 0.25 + 0.25) +
          "s alternate infinite ease-in";
      }
    }
  };
  if (audio.paused) {
    animationValidation(false);
    audio.play();

    document.querySelector("#play img").src = "media/pause.png";
  } else {
    animationValidation(true);
    audio.pause();
    document.querySelector("#play img").src = "media/play.png";
  }
}

// this function is fired when we click on next to load the next song
function next() {
  renderWaveForm(audio["duration"]);
  audioIndex++;
  audioIndex %= audios.length;
  loadAudio();
  document.querySelector("#play img").src = "media/pause.png";
}

// this function is fired when we click on previous to load the previous song
function previous() {
  audioIndex--;
  audioIndex = audioIndex > -1 ? audioIndex : audios.length - 1;
  loadAudio();
  document.querySelector("#play img").src = "media/pause.png";
}

audio.addEventListener("loadeddata", function() {
  renderWaveForm(audio["duration"]);
});
// we initialize our audio file and we inject it into the DOM
window.onload = loadAudio();
document.body.appendChild(audio);

// When the document is loaded we add our waveform
window.addEventListener("load", function() {
  duration = audio["duration"];
  loadTime(duration);
  renderWaveForm(duration);
});

// when the window is resized, we change automatically the waveform size
window.addEventListener("resize", function() {
  // add subtitle to mobile version
  if (window.innerWidth < 600) window.resizeTo(600, window.innerHeight);

  var mmn = get("MobileMusicName");
  if (mmn != null) mmn.innerHTML = cards[audioIndex]["music"];
  contentwidth = get("content").offsetWidth - 42;
  var bars = document.getElementsByClassName("fragment");
  for (bar of bars) {
    bar.style.width = contentwidth / audio["duration"];
  }
});

// this function is used to display the duration and the audio current time with the format mm:ss
var loadTime = function(duration) {
  var timeLabel = String(audio.currentTime).formatTime();
  var durationLabel = String(audio.duration).formatTime();

  get("time").innerHTML = timeLabel;
  get("barTime").innerHTML = timeLabel;
  if (isNaN(audio.duration) == false) {
    get("duration").innerHTML = durationLabel;
    get("barDuration").innerHTML = durationLabel;
  }
};

// Here we define the function that renders the waveForm
var renderWaveForm = function(duration) {
  var content = get("content");
  if (content != null) content.innerHTML = "";
  var contentwidth = content.offsetWidth - 42;
  for (i = 0; i <= audio["duration"]; i++) {
    // create a child
    var child = document.createElement("div");
    child.style.height = 20 + parseInt(Math.random(0, 1) * 30);
    child.className = "fragment";
    child.id = i;
    child.style.width = contentwidth / audio["duration"];

    // add keyframes unique for each bar
    var from = child.style.height;
    var to = Math.random(0, 1) * (1.3 - 1.0) + 1.0;
    var keyFrame =
      "@keyframes kf" +
      i +
      " { 0% {transform: scale(" +
      from +
      ");} 100% {transform: scale(" +
      to +
      ");}}";
    document.styleSheets[0].insertRule(keyFrame);

    // add listener to the cild
    child.addEventListener("click", function() {
      audio.currentTime = parseInt(this.id);
      if (audio.paused) {
        audio.play();
        document.querySelector("#play img").src = "media/pause.png";
      }
      for (j = 0; j <= parseInt(this.id); j++) {
        get(j).style.backgroundColor = "#8E3EAE";
        get(j).style.animation =
          "kf" +
          j +
          " " +
          (Math.random(0, 1) * 0.25 + 0.25) +
          "s alternate infinite ease-in";
      }
      for (j = parseInt(this.id) + 1; j < audio["duration"]; j++) {
        if (get(j)) {
          get(j).style.backgroundColor = "#707070";
          get(j).style.animation = null;
        }
      }
    });
    // append the created child to the content
    get("content").appendChild(child);
  }
};
// when the audio is finished we reset our waveForm parameters
audio.addEventListener("ended", function() {
  document.querySelector("#play img").src = "media/play.png";
  for (j = 0; j <= audio["duration"]; j++) {
    if (get(j)) {
      get(j).style.backgroundColor = "#707070";
      get(j).style.animation = null;
    }
  }
});

// when the audio current time is updated, we change bar color related to the audio currentTime and we remove the animation as well.
audio.addEventListener("timeupdate", function() {
  // first we load time

  loadTime(audio["duration"]);
  var currentTime = parseInt(audio["currentTime"]);
  for (j = 0; j <= currentTime; j++) {
    if (get(j)) {
      get(j).style.backgroundColor = "#8E3EAE";
      var animation = get(j).style.animation.split(" ");

      if (animation[animation.length - 1] == "none") {
        get(j).style.animation =
          "kf" +
          j +
          " " +
          (Math.random(0, 1) * 0.25 + 0.25) +
          "s alternate infinite ease-in";
      }
    }
  }
  for (j = currentTime + 1; j <= audio["duration"]; j++) {
    if (get(j) != null) {
      get(j).style.backgroundColor = "#707070";
      get(j).style.animation = "none";
    }
  }
});

// a function to format the currentTime and duration
String.prototype.formatTime = function() {
  var time = parseInt(this, 10);
  var minutes = Math.floor(time / 60);
  var seconds = time - minutes * 60;

  var addZero = p => {
    return p < 10 ? "0" + p : p;
  };
  minutes = addZero(minutes);
  seconds = addZero(seconds);

  return minutes + ":" + seconds;
};

var get = id => document.getElementById(id);
