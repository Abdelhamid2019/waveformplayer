window.addEventListener("load", function() {
  var windowWidth = window.innerWidth;
  var time = document.createElement("div");
  time.id = "time";
  time.className = "time";

  var content = document.createElement("div");
  content.id = "content";

  var duration = document.createElement("div");
  duration.id = "duration";
  duration.className = "time";

  if (windowWidth > 1100) {
    var waveform = get("desktopWaveForm");

    // add waveformDesktop (time,content,duration)
    waveform.appendChild(time);
    waveform.appendChild(content);
    waveform.appendChild(duration);
    // remove waveformMobile
    get("mobileContainer").outerHTML = "";
    // remove mobileTitle
    get("mobileTitle").outerHTML = "";
    // adjust the waveform size
    get("content").style.width = window.innerWidth - 690 + "px";
    get("barContainer").style.display = "none";
  } else {
    get("barContainer").style.display = "flex";
    get("desktopWaveForm").outerHTML = "";
    var mobilecontainer = get("mobileContainer");
    // remove volume button
    get("volume").style.display = "none";
    // remove the music name and readjust the margin of the artist name!
    get("musicName").style.display = "none";
    get("artistName").style.marginTop = 0;
    get("timeDuration").appendChild(time);
    get("mobileContent").appendChild(content);
    get("timeDuration").appendChild(duration);
    // remove the waveForm including time and duration
    get("content").style.width = window.innerWidth - 30 + "px";
    // the left and the right part of the player should have the same size
    get("playerRightPart").style.width = 170+"px";
    swipe("mobileContainer");
  }
});

window.addEventListener("resize", function() {
  if (window.innerWidth > 1100) {
    get("barContainer").style.display = "none";
    var time = document.createElement("div");
    time.id = "time";
    time.className = "time";

    var content = document.createElement("div");
    content.id = "content";

    var duration = document.createElement("div");
    duration.id = "duration";
    duration.className = "time";

    var mobileContainer = get("mobileContainer");

    if (mobileContainer != null) mobileContainer.outerHTML = "";

    // create the insert the waveForm - desktop version
    if (get("desktopWaveForm") == null) {
      document
        .getElementById("control")
        .insertAdjacentHTML("afterend", "<div id='desktopWaveForm'></div>");

      var desktopWaveForm = get("desktopWaveForm");
      desktopWaveForm.appendChild(time);
      desktopWaveForm.appendChild(content);
      desktopWaveForm.appendChild(duration);
    }

    if (get("content").innerHTML == "") {
      loadTime(audio["duration"]);
      renderWaveForm(audio["duration"]);
      /* check the animation if the music is played */
      if (audio.paused == false) {
        for (i = 0; i <= audio.currentTime; i++) {
          get(i).style.animation =
            "kf" +
            i +
            " " +
            (Math.random(0, 1) * 0.25 + 0.25) +
            "s alternate infinite ease-in";
        }
      }
    }

    get("artistName").style.marginTop = 0;

    var mobileTitleTemp = get("mobileTitle");
    if (mobileTitleTemp != null) mobileTitleTemp.outerHTML = "";
    get("volume").style.display = "block";
    get("musicName").style.display = "block";
    get("content").style.width = window.innerWidth - 690 + "px";
  } else {
    get("barContainer").style.display = "flex";
    // remove desktopwaveform
    if (get("desktopWaveForm") != null) get("desktopWaveForm").outerHTML = "";

    // create mobile Container again
    if (get("mobileContainer") == null) {
      // for simplicity we use only in here the outerHTML instead of createElement Function
      var mobileContainer =
        '<div id="mobileContainer"><div id="mobileContainerHeader"></div><div id="timeDuration"style="display:flex;width:100%;justify-content:space-between;"><div id="time" class="time"></div><div id="duration" class="time"></div></div><div id="mobileContent"></div></div>';

      var mobileTitle =
        '<div id="mobileTitle"><p id="MobileMusicName">Halo - Sasha Fierce</p></div>';

      get("barContainer").insertAdjacentHTML("afterend", mobileTitle);
      get("container").insertAdjacentHTML("beforebegin", mobileContainer);

      var mobileContent = get("mobileContent");

      mobileContent.insertAdjacentHTML("beforeend", '<div id="content"></div>');
    }

    if (get("content").innerHTML == "") {
      loadTime(audio["duration"]);
      renderWaveForm(audio["duration"]);
      if (audio.paused == false) {
        for (i = 0; i <= audio.currentTime; i++) {
          get(i).style.animation =
            "kf" +
            i +
            " " +
            (Math.random(0, 1) * 0.25 + 0.25) +
            "s alternate infinite ease-in";
        }
      }
    }
    // remove volume and title
    get("volume").style.display = "none";
    get("musicName").style.display = "none";
    swipe("mobileContainer");
    get("content").style.width = window.innerWidth - 30 + "px";
  }
});
window.onload = swipe("mobileContainer");

window.addEventListener("load", function() {
  var barPlayer = () => {
    var ct = audio.currentTime;
    var styles = window.getComputedStyle;
    var width = id =>
      Number(window.getComputedStyle(get(id)).width.replace("px", ""));

    var seekBar = get("seekBar");
    var progress = get("progress");
    var barControl = get("barControl");

    var size = (ct * width("seekBar")) / audio.duration;
    progress.style.width = size;
    barControl.style.left = size;
  };
  // I call the function
  barPlayer();
  // update the barPlayer for everychange
  get("barTime").innerHTML = audio.addEventListener("timeupdate", function() {
    barPlayer();
  });
});

var get = id => document.getElementById(id);
